/* eslint-disable no-nested-ternary */
import React from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import Button from "../Button";
import { nominalCleaner } from "../util";
import { requestThunk } from "../../modules/group/request/thunk";

const CardStyle = styled.div`
  width: 100%;
  background-color: #fbfbfb;
  border-radius: 5px;
  display: flex;
  margin-bottom: 10px;
  aling-items: center;
  justify-content: space-between;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  p {
    margin: 4px;
  }

  .action-button {
    display: flex;
  }

  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const Card = props => {
  const { data, username } = props;
  const { title, nominal, group, isPaid, isVerified, id } = data;
  const dispatch = useDispatch();

  const pay = () => {
    console.log(username, id);
    dispatch(requestThunk.payTuition(username, id));
  };

  return (
    <CardStyle>
      <div>
        <p>{title}</p>
        <p>Group: {group}</p>
        <p>Rp {nominalCleaner(nominal)}</p>
        <p>
          Status:{" "}
          {isPaid
            ? isVerified
              ? "Sudah Diverifikasi"
              : "Menunggu Verifikasi"
            : "Belum Dibayar"}
        </p>
      </div>
      <div className="action-button">
        <Button
          value={isPaid ? "Batalkan Laporan" : "Lapor Sudah Bayar"}
          colorBg={isPaid ? "#ffbb00" : "gray"}
          color="white"
          onClick={() => pay()}
        />
      </div>
    </CardStyle>
  );
};

export default Card;
