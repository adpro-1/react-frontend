import React from "react";
import { useSelector } from "react-redux";
import { Container } from "./style";
import Card from "./card";

const Tuition = props => {
  const { username } = props;
  const { tuition } = useSelector(state => ({
    tuition: state.group.tuition
  }));

  const renderCard = () => {
    if (tuition.length > 0) {
      return tuition.map(t => {
        return <Card data={t} username={username} />;
      });
    }
    return <h2>Selamat anda tidak punya hutang :)</h2>;
  };

  return <Container>{renderCard()}</Container>;
};

export default Tuition;
