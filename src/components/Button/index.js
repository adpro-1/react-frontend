import React from "react";
import { ButtonContainer } from "./style";

class Button extends React.Component {
  onClick = () => {
    const { onClick } = this.props;
    if (onClick) {
      onClick();
    }
  };

  render() {
    const { value, color, colorBg, disabled } = this.props;
    return (
      <ButtonContainer>
        <button
          style={{
            backgroundColor: `${colorBg}`,
            color: `${color}`,
            ...this.props
          }}
          type="button"
          onClick={this.onClick}
          disabled={disabled}
        >
          {value}
        </button>
      </ButtonContainer>
    );
  }
}

Button.propTypes = {};

export default Button;
