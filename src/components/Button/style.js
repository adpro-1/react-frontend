import styled from "styled-components";

export const ButtonContainer = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: center;
  padding: 10px;
  button {
    border: none;
    margin-left: 5px;
    margin-right: 5px;
    border-radius: 5px;
    padding: 10px;
    min-width: 100px;
  }
`;
