import React from "react";
import styled from "styled-components";
import Button from "../Button";

const Container = styled.div`
  position: fixed;
  display: flex;
  z-index: 7;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  bottom: 0;
  left: 0;

  .row {
    display: flex;
    justify-content: space-around;
  }
`;

const White = styled.div`
  background-color: white;
  padding: 50px;
  border-radius: 5px;
  z-index: 10;
`;

const Black = styled.div`
  background-color: black;
  position: fixed;
  opacity: 0.5;
  height: 100vh;
  width: 100vw;
  bottom: 0;
`;

const Modal = props => {
  const { children, label } = props;
  return (
    <Container>
      <Black />
      <White>
        {children}
        <div className="row">
          <Button value={label} onClick={() => props.submit()} />
          <Button value="Batal" onClick={() => props.closeModal()} />
        </div>
      </White>
    </Container>
  );
};

export default Modal;
