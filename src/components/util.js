export const nominalCleaner = number => {
  const str = number.toString();
  let res = "";
  let counter = 0;
  for (let i = str.length - 1; i >= 0; i -= 1) {
    if (counter < 3) {
      res = str[i] + res;
      counter += 1;
    } else {
      res = `${str[i]}.${res}`;
      counter = 1;
    }
  }
  return res;
};

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "Mei",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Okt",
  "Nov",
  "Des"
];

export const cleanTime = time => {
  const date = new Date(time);
  return `${date.getDate()} ${
    months[date.getMonth()]
  }, ${date.getFullYear()} at ${date.getHours()}:${date.getMinutes()} WIB`;
};
