import styled from "styled-components";

export const Container = styled.div`
  height: 290px;
  border-radius: 5px;
  background-color: #ffbb00;
  padding: 20px;
  color: white;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  .code-text {
    display: flex;
    align-items: center;s
  }
`;
