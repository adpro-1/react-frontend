import React from "react";
import { Container } from "./style";
import Button from "../../Button";

const Profile = props => {
  const nominalCleaner = number => {
    const str = number.toString();
    let res = "";
    let counter = 0;
    for (let i = str.length - 1; i >= 0; i -= 1) {
      if (counter < 3) {
        res = str[i] + res;
        counter += 1;
      } else {
        res = `${str[i]}.${res}`;
        counter = 1;
      }
    }
    return res;
  };

  const copyCodeToClipboard = () => {
    const val = document.getElementById("code");
    const range = document.createRange();
    range.selectNode(val);
    window.getSelection().addRange(range);
    document.execCommand("copy");
  };

  const { name, budget, code } = props;
  return (
    <Container>
      <h1>{name}</h1>
      {budget && <h2>Budget : {`Rp ${nominalCleaner(budget)}`}</h2>}
      <h4 className="code-text">
        Unique Code : <span id="code">{code}</span>
        <Button
          onClick={() => copyCodeToClipboard()}
          value="copy"
          min-width="0"
        />
      </h4>
    </Container>
  );
};

export default Profile;
