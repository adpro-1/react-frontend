import React from "react";
import styled from "styled-components";
import Admin from "./Admin";
import Member from "./Member";

const Container = styled.div`
  width: 291px;
  height: 100%;
  background-color: #fbfbfb;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  display: flex;
  flex-direction: column;
  border-radius: 5px;
  margin-right: 40px;

  .box {
    padding: 20px;
    display: flex;
    flex-direction: column;
    height: 200px;
    overflow-y: scroll;
  }

  .title {
    text-align: center;
  }

  @media only screen and (max-width: 768px) {
    width: 100%;
    margin-bottom: 40px;
  }
`;

const Members = props => {
  const { admin, member, username, id } = props;

  return (
    <Container>
      <div className="box">
        <h3 className="title">Admin</h3>
        <Admin data={admin} />
      </div>
      <div className="box">
        <h3 className="title">Member</h3>
        <Member data={member} admin={username} id={id} />
      </div>
    </Container>
  );
};

export default Members;
