import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
`;

export const CardContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  .action-btn {
    display: flex;

    button {
      border: none;
    }
  }
`;
