/* eslint-disable react/destructuring-assignment */
import React from "react";
import { Container, CardContainer } from "./style";
// import userLogo from "../../../../static/user-x.svg"
// import Button from "../../../Button";

const Card = ({ user }) => {
  const { username } = user;

  return (
    <CardContainer>
      <p>{username}</p>
      {/* <div className="action-btn">
        <Button value={<img src={userLogo} alt="user" />} min-width="0" margin="0" colorBg="transparent" />
      </div> */}
    </CardContainer>
  );
};

const Admin = props => {
  return (
    <Container>
      {props.data.map(user => {
        return <Card user={user} />;
      })}
    </Container>
  );
};

export default Admin;
