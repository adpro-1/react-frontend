/* eslint-disable react/destructuring-assignment */
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, CardContainer } from "./style";
import { cekIsAdmin } from "../../../../modules/group/crud/selectors";
import userLogo from "../../../../static/user.svg";
import removeLogo from "../../../../static/delete.svg";
import Button from "../../../Button";
import { crudThunk } from "../../../../modules/group/crud/thunk";
import { requestThunk } from "../../../../modules/group/request/thunk";

const Card = props => {
  const { user, admin, id } = props;
  const { username } = user;
  const { isAdmin } = useSelector(state => ({
    isAdmin: cekIsAdmin(state.group.groupDetail, admin)
  }));
  const dispatch = useDispatch();

  return (
    <CardContainer>
      <p>{username}</p>
      {isAdmin && (
        <div className="action-btn">
          <Button
            value={<img src={userLogo} alt="user" />}
            min-width="0"
            margin="0"
            colorBg="transparent"
            onClick={() =>
              dispatch(requestThunk.inviteAdmin(admin, id, username))
            }
          />
          <Button
            value={<img src={removeLogo} alt="user" />}
            min-width="0"
            margin="0"
            colorBg="transparent"
            onClick={() =>
              dispatch(crudThunk.removeMember(admin, id, username))
            }
          />
        </div>
      )}
    </CardContainer>
  );
};

const Member = props => {
  const { admin, id } = props;
  return (
    <Container>
      {props.data.map(user => {
        return <Card user={user} admin={admin} id={id} />;
      })}
    </Container>
  );
};

export default Member;
