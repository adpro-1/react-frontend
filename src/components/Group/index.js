import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import Members from "./Members";
import Action from "./Action";
import Profile from "./Profile";

const Container = styled.div`
  width: 100%;

  .row {
    margin-top: 40px;
    display: flex;
    flex-direction: row;
    margin-buttom: 40px;
  }

  @media only screen and (max-width: 768px) {
    .row {
      flex-direction: column;
    }
  }
`;

const Group = props => {
  const { data } = props;
  const { username, id } = data;
  const { group, loading } = useSelector(state => ({
    group: state.group.groupDetail,
    loading: state.group.loading
  }));
  return (
    <Container>
      {loading && <h1>Loading...</h1>}
      {!loading && (
        <>
          <Profile name={group.name} budget={group.budget} code={group.code} />
          {group.admins && group.members && (
            <div className="row">
              <Members
                admin={group.admins}
                member={group.members}
                username={username}
                id={id}
              />
              <Action
                username={username}
                id={id}
                code={group.code}
                budget={group.budget}
                tuition={group.tuition}
              />
            </div>
          )}
        </>
      )}
    </Container>
  );
};

export default Group;
