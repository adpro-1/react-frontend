import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 5px;
  background-color: #fbfbfb;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  .action {
    display: flex;
    padding: 20px;
    align-items: center;
    justify-content: center;
  }

  .tab {
    width: 100%;
    display: flex;
    justify-content: center;
    padding: 10px;
    color: black;
    background-color: white;
    border-radius: 5px;
  }

  .active {
    background-color: orange;
    color: white;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  }

  @media only screen and (max-width: 768px) {
    margin-bottom: 40px;
  }
`;
