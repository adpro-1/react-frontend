/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Container } from "./style";
import AdminAction from "./AdminAction";
import { cekIsAdmin } from "../../../modules/group/crud/selectors";
import List from "./List";
import Button from "../../Button";
import RequestBudgetForm from "./RequestBudgetForm";
import Tuition from "./Tuition";
import CreateTuitionForm from "./CreateTuitionForm";

const Action = props => {
  const { username, id, code, tuition } = props;
  const { isAdmin } = useSelector(state => ({
    isAdmin: cekIsAdmin(state.group.groupDetail, username)
  }));
  const [isOpen, setIsOpen] = useState(false);
  const [isActive, setIsActive] = useState(1);

  const onClick = () => {
    setIsOpen(true);
  };

  const close = () => {
    setIsOpen(false);
  };
  console.log(username, id);
  return (
    <Container>
      <div className="action">
        <div
          tabIndex={0}
          className={`tab ${isActive === 1 && "active"}`}
          onClick={() => setIsActive(1)}
        >
          Dashboard
        </div>
        {isAdmin && (
          <div
            tabIndex={0}
            className={`tab ${isActive === 2 && "active"}`}
            onClick={() => setIsActive(2)}
          >
            Iuran
          </div>
        )}
      </div>
      <div className="action">
        <Button
          value={isActive === 1 ? "Request Budget" : "Buat Tagihan"}
          onClick={() => onClick()}
        />
        {isAdmin && <AdminAction id={id} username={username} />}
      </div>
      {isActive === 2 && (
        <Tuition tuition={tuition} username={username} id={id} />
      )}
      {isActive === 1 && <List id={id} username={username} />}
      {isOpen && isActive === 1 && (
        <RequestBudgetForm
          close={close}
          username={username}
          code={code}
          id={id}
        />
      )}
      {isOpen && isActive === 2 && (
        <CreateTuitionForm close={close} username={username} id={id} />
      )}
    </Container>
  );
};

export default Action;
