import React, { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { requestThunk } from "../../../../modules/group/request/thunk";
import { cekIsAdmin } from "../../../../modules/group/crud/selectors";
import Button from "../../../Button";
import { cleanTime } from "../../../util";

const Container = styled.div`
  border-radius: 5px;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 20px;

  .content {
    border-radius: 5px;
    padding: 10px;
    overflow-y: scroll;
    height: 230px;
    background-color: white;
    margin: 5px;
  }
`;

const CardStyle = styled.div`
  width: 100%;
  background-color: #fbfbfb;
  border-radius: 5px;
  display: flex;
  margin-bottom: 5px;
  aling-items: center;
  justify-content: space-between;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  p {
    margin: 4px;
  }

  .action-button {
    display: flex;
  }

  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const Card = props => {
  const { data, admin, type, groupId, isAdmin } = props;
  const { username } = data.user;
  const { timestamp, id } = data;
  const dispatch = useDispatch();

  const approve = status => {
    if (type === "budget") {
      dispatch(requestThunk.approveRequestBudget(admin, id, status, groupId));
    } else {
      dispatch(
        requestThunk.approveMembershipRequest(admin, id, status, groupId)
      );
    }
  };

  const nominalCleaner = number => {
    const str = number.toString();
    let res = "";
    let counter = 0;
    for (let i = str.length - 1; i >= 0; i -= 1) {
      if (counter < 3) {
        res = str[i] + res;
        counter += 1;
      } else {
        res = `${str[i]}.${res}`;
        counter = 1;
      }
    }
    return res;
  };

  return (
    <CardStyle>
      <div>
        <p>
          {username}
          {type === "budget" && ` - Rp ${nominalCleaner(data.amount)}`}
        </p>
        <p>{cleanTime(timestamp)}</p>
      </div>
      {isAdmin && (
        <div className="action-button">
          <Button
            value="Reject"
            colorBg="red"
            color="white"
            onClick={() => approve(1)}
          />
          <Button
            value="Accept"
            colorBg="green"
            color="white"
            onClick={() => approve(2)}
          />
        </div>
      )}
    </CardStyle>
  );
};

const List = props => {
  const dispatch = useDispatch();
  const { username, id } = props;
  const { membership, budget, isAdmin } = useSelector(state => ({
    membership: state.group.request.membershipRequest,
    budget: state.group.request.requestBudget,
    isAdmin: cekIsAdmin(state.group.groupDetail, username)
  }));

  const renderRequest = (data, type) => {
    const list = data.map(r => {
      return (
        <Card
          data={r}
          admin={username}
          isAdmin={isAdmin}
          groupId={id}
          type={type}
        />
      );
    });
    return list;
  };

  useEffect(() => {
    dispatch(requestThunk.getGroupRequest(username, id));
  }, [dispatch]);

  return (
    <Container>
      <h4>Budget Request</h4>
      <div className="content">{budget && renderRequest(budget, "budget")}</div>
      {isAdmin && (
        <>
          <h4>Membership Request</h4>
          <div className="content">
            {membership && renderRequest(membership, "membership")}
          </div>
        </>
      )}
    </Container>
  );
};

export default List;
