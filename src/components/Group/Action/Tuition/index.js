import React, { useState } from "react";
import styled from "styled-components";
import Card from "./card";
import TuitionDetail from "./tuitionDetail";

const Container = styled.div`
  padding: 30px;
`;

const Tuition = props => {
  const { tuition, username, id } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [tuitionData, setTuitionData] = useState([]);

  const open = e => {
    setTuitionData(e);
    setIsOpen(!isOpen);
  };

  const renderCard = () => {
    return tuition.map(t => {
      return (
        <Card
          title={t.title}
          nominal={t.nominal}
          open={e => open(e)}
          data={t}
        />
      );
    });
  };

  return (
    <Container>
      {isOpen && (
        <TuitionDetail
          close={open}
          data={tuitionData}
          admin={username}
          groupId={id}
        />
      )}
      {tuition && renderCard()}
    </Container>
  );
};

export default Tuition;
