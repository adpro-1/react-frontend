import React from "react";
import styled from "styled-components";
import Button from "../../../Button";

const CardStyle = styled.div`
  width: 100%;
  background-color: white;
  border-radius: 5px;
  display: flex;
  padding: 5px;
  margin-bottom: 5px;
  aling-items: center;
  justify-content: space-between;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  p {
    margin: 4px;
  }

  .action-button {
    display: flex;
  }

  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const Card = props => {
  const { title, nominal, open, data } = props;

  const nominalCleaner = number => {
    const str = number.toString();
    let res = "";
    let counter = 0;
    for (let i = str.length - 1; i >= 0; i -= 1) {
      if (counter < 3) {
        res = str[i] + res;
        counter += 1;
      } else {
        res = `${str[i]}.${res}`;
        counter = 1;
      }
    }
    return res;
  };

  return (
    <CardStyle>
      <div>
        <p>{title}</p>
        <p>Rp {nominalCleaner(nominal)}</p>
      </div>
      <div className="action-button">
        <Button
          value="Kelola"
          colorBg="green"
          color="white"
          onClick={() => open(data)}
        />
      </div>
    </CardStyle>
  );
};

export default Card;
