/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import Button from "../../../Button";
import { nominalCleaner, cleanTime } from "../../../util";
import { requestThunk } from "../../../../modules/group/request/thunk";

const Container = styled.div`
  z-index: 20;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  position: fixed;
  bottom: 0;
  left: 0;

  .black {
    background-color: black;
    position: fixed;
    opacity: 0.5;
    height: 100vh;
    width: 100vw;
    bottom: 0;
    left: 0;
  }

  .box {
    background-color: white;
    padding: 10px;
    width: 80vw;
    height: 80vh;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    // justify-content: center;

    h2 {
      margin: 2px;
    }
    h3 {
      margin: 2px;
    }

    .close {
      position: absolute;
      top: 10px;
      right: 10px;

      h3 {
        margin: 0;
      }
    }

    .list {
      width: 100%;
      height: 70vh;
      overflow-y: scroll;
      padding: 10px;
    }
  }
`;

const CardStyle = styled.div`
  width: 100%;
  background-color: #fbfbfb;
  border-radius: 5px;
  display: flex;
  margin-bottom: 5px;
  aling-items: center;
  justify-content: space-between;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  p {
    margin: 4px;
  }

  .action-button {
    display: flex;
  }

  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const Card = props => {
  const { data, groupId, admin } = props;
  const { user, isPaid, isVerified, timestamp, id } = data;
  const dispatch = useDispatch();

  const verify = () => {
    console.log(admin, id, groupId);
    dispatch(requestThunk.verifyPayTuition(admin, id, groupId));
  };
  return (
    <CardStyle>
      <div>
        <p>{user}</p>
        <p>Status: {isPaid ? "Sudah Bayar" : "Belum Bayar"}</p>
        {!isPaid && <p>Ditagih pada {cleanTime(timestamp)}</p>}
        {isPaid && !isVerified && <p>Dibayar pada {cleanTime(timestamp)}</p>}
        {isVerified && <p>Diverifikasi pada {cleanTime(timestamp)}</p>}
      </div>
      <div className="action-button">
        <Button
          value={
            isPaid
              ? isVerified
                ? "Sudah Diverifikasi"
                : "Verifikasi"
              : "Belum Dibayar"
          }
          colorBg={isPaid ? (isVerified ? "green" : "#ffbb00") : "gray"}
          color="white"
          disabled={isVerified || !isPaid}
          onClick={() => verify()}
        />
      </div>
    </CardStyle>
  );
};

const CloseButton = props => {
  const { onClick } = props;
  return (
    <div tabIndex={0} role="button" className="close" onClick={() => onClick()}>
      <h3>X</h3>
    </div>
  );
};

const List = props => {
  const { data, admin, groupId } = props;

  const renderCard = () => {
    return data.map(c => {
      return <Card data={c} admin={admin} groupId={groupId} />;
    });
  };

  return <div className="list">{renderCard()}</div>;
};

const TuitionDetail = props => {
  const { close, data, groupId, admin } = props;
  const { isNotified, title, nominal, memberTuitions, id } = data;
  const dispatch = useDispatch();

  const notify = () => {
    dispatch(requestThunk.notifyTuition(admin, id, groupId));
  };

  return (
    <Container>
      <div className="black" />
      <div className="box">
        <CloseButton onClick={close} />
        <h2>{title}</h2>
        <h3>Rp {nominalCleaner(nominal)}</h3>
        <div>
          <Button
            value={isNotified ? "Tagih Lagi" : "Tagih"}
            colorBg="#ffbb00"
            color="white"
            onClick={() => notify()}
          />
          {!isNotified && <h4>Tagihan belum ditagih ke anggota</h4>}
        </div>
        <List data={memberTuitions} groupId={groupId} admin={admin} />
      </div>
    </Container>
  );
};

export default TuitionDetail;
