import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "../../../Modal";
import Form from "../../../Form";
import { crudThunk } from "../../../../modules/group/crud/thunk";

const CreateTuitionForm = props => {
  const { close, id, username } = props;
  const dispatch = useDispatch();
  const [amount, setAmount] = useState(0);
  const [title, setTitle] = useState("");

  const amountHandler = val => {
    setAmount(val);
  };

  const titleHandler = val => {
    setTitle(val);
  };

  const submit = () => {
    dispatch(crudThunk.createTuitiom(username, id, title, amount));
  };

  return (
    <Modal closeModal={close} label="Buat Tagihan" submit={submit}>
      <Form label="title" handler={val => titleHandler(val)} />
      <Form label="Nominal" type="number" handler={val => amountHandler(val)} />
    </Modal>
  );
};

export default CreateTuitionForm;
