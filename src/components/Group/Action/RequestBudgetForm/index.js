import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Modal from "../../../Modal";
import Form from "../../../Form";
import { requestThunk } from "../../../../modules/group/request/thunk";

const RequestBudgetForm = props => {
  const { close, code, id, username } = props;
  const dispatch = useDispatch();
  const [amount, setAmount] = useState(0);
  const { loading } = useSelector(state => ({
    loading: state.group.loadingRequest || state.group.loading
  }));

  const amountHandler = val => {
    setAmount(val);
  };

  const submit = () => {
    dispatch(requestThunk.requestBudget(username, code, amount, id));
  };

  return (
    <Modal closeModal={close} label="Request" submit={submit}>
      {!loading && <Form label="Amount" handler={val => amountHandler(val)} />}
      {loading && <h3>Requesting...</h3>}
    </Modal>
  );
};

export default RequestBudgetForm;
