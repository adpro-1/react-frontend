import React, { useState } from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import Button from "../../../Button";
import { crudThunk } from "../../../../modules/group/crud/thunk";
import AddMemberForm from "./AddMemberForm";
import AddBudgetForm from "./AddBudgetForm";

const Container = styled.div`
  .button {
    display: flex;
  }

  @media only screen and (max-width: 768px) {
    overflow-x: scroll;
  }
`;

const AdminAction = props => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [choice, setChoice] = useState(0);
  const { id, username } = props;
  const deleteGroup = () => {
    dispatch(crudThunk.deleteGroup(username, id));
    window.location.href = "/";
  };

  const onClick = number => {
    setIsOpen(true);
    setChoice(number);
  };

  const close = () => {
    setIsOpen(false);
  };

  return (
    <Container>
      <div className="button">
        <Button value="Tambah Member" onClick={() => onClick(1)} />
        <Button value="Tambah Budget" onClick={() => onClick(2)} />
        <Button
          onClick={deleteGroup}
          value="delete"
          colorBg="red"
          color="white"
        />
      </div>
      {isOpen && choice === 1 && (
        <AddMemberForm close={close} id={id} admin={username} />
      )}
      {isOpen && choice === 2 && (
        <AddBudgetForm close={close} id={id} admin={username} />
      )}
    </Container>
  );
};

export default AdminAction;
