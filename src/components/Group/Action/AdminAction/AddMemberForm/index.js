import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "../../../../Modal";
import Form from "../../../../Form";
import { requestThunk } from "../../../../../modules/group/request/thunk";

const AddMemberForm = props => {
  const { close, id, admin } = props;
  const [username, setUsername] = useState("");
  const dispatch = useDispatch();

  const usernameHandler = val => {
    setUsername(val);
  };

  const submit = () => {
    dispatch(requestThunk.inviteMember(admin, id, username));
  };

  return (
    <Modal label="Tambah" closeModal={close} submit={submit}>
      <Form label="Username" handler={val => usernameHandler(val)} />
    </Modal>
  );
};

export default AddMemberForm;
