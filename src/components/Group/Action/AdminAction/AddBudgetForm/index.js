import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "../../../../Modal";
import Form from "../../../../Form";
import { crudThunk } from "../../../../../modules/group/crud/thunk";

const AddBudgetForm = props => {
  const { close, id, admin } = props;
  const [nominal, setNominal] = useState(0);
  const dispatch = useDispatch();

  const nominalHandler = val => {
    setNominal(val);
  };

  const submit = () => {
    dispatch(crudThunk.editBudget(admin, id, nominal));
  };

  return (
    <Modal label="Tambah" closeModal={close} submit={submit}>
      <Form
        type="number"
        label="Nominal"
        handler={val => nominalHandler(val)}
      />
    </Modal>
  );
};

export default AddBudgetForm;
