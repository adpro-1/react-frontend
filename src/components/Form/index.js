import React from "react";
import { FormContainer } from "./style";

class Form extends React.Component {
  onChange = e => {
    const { handler } = this.props;
    handler(e.target.value);
  };

  render() {
    const { type, label } = this.props;
    return (
      <FormContainer>
        <span>{label}</span>
        <input type={type} onChange={this.onChange} />
      </FormContainer>
    );
  }
}

Form.propTypes = {};

export default Form;
