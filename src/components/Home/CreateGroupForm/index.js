import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Form from "../../Form";
import { crudThunk } from "../../../modules/group/crud/thunk";
import { getUsername } from "../../../modules/auth/login/selectors";
import Modal from "../../Modal";

const CreateGroupForm = props => {
  const { close } = props;
  const [name, setName] = useState("");
  const [budget, setBudget] = useState(0);
  const username = useSelector(state => getUsername(state));
  const dispatch = useDispatch();

  const nameHandler = nameProps => {
    setName(nameProps);
  };

  const budgetHandler = budgetProps => {
    setBudget(budgetProps);
  };

  const submit = () => {
    setBudget(0);
    setName("");
    props.close();
    dispatch(crudThunk.createGroup(username, name, budget));
  };

  return (
    <Modal label="Buat Group" submit={submit} closeModal={close}>
      <Form
        type="text"
        label="Name"
        handler={nameProps => nameHandler(nameProps)}
      />
      <Form
        type="number"
        label="Budget Awal"
        handler={budgetProps => budgetHandler(budgetProps)}
      />
    </Modal>
  );
};

export default CreateGroupForm;
