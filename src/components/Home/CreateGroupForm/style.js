import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  display: flex;
  z-index: 7;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  bottom: 0;

  .row {
    display: flex;
    justify-content: space-around;
  }
`;

export const FormContainer = styled.div`
  background-color: white;
  padding: 100px;
  border-radius: 5px;
  z-index: 10;
`;

export const Black = styled.div`
  background-color: black;
  position: fixed;
  opacity: 0.5;
  height: 100vh;
  width: 100vw;
  bottom: 0;
`;
