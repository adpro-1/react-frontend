import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Form from "../../Form";
import { requestThunk } from "../../../modules/group/request/thunk";
import { getUsername } from "../../../modules/auth/login/selectors";
import Modal from "../../Modal";

const JoinGroupForm = props => {
  const { close } = props;
  const [code, setCode] = useState("");
  const username = useSelector(state => getUsername(state));
  const dispatch = useDispatch();

  const codeHandler = codeProps => {
    setCode(codeProps);
  };

  const submit = () => {
    setCode("");
    props.close();
    dispatch(requestThunk.createMembershipRequest(username, code));
  };

  return (
    <Modal label="Gabung Group" submit={submit} closeModal={close}>
      <Form
        type="text"
        label="Group Code"
        handler={codeProps => codeHandler(codeProps)}
      />
    </Modal>
  );
};

export default JoinGroupForm;
