import React from "react";
import { Link } from "react-router-dom";
import { Container } from "./style";

const GroupCard = props => {
  const { name, code, id } = props;
  return (
    <Link
      to={`/${id}/${name}`}
      style={{ textDecoration: "none", color: "black" }}
    >
      <Container>
        <h3>Nama: {name}</h3>
        <h3>Code: {code}</h3>
      </Container>
    </Link>
  );
};

export default GroupCard;
