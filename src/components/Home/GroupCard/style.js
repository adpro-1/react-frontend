import styled from "styled-components";

export const Container = styled.div`
  background-color: lightgray;
  border-radius: 5px;
  padding: 20px;
  flex-grow: 1;
  width: 325px;
  margin: 5px;
`;
