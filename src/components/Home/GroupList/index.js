/* eslint-disable react/no-array-index-key */
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, List } from "./style";
import { getUsername } from "../../../modules/auth/login/selectors";
import { crudThunk } from "../../../modules/group/crud/thunk";
import GroupCard from "../GroupCard";

const GroupList = () => {
  const dispatch = useDispatch();
  const { loading, error, group, username } = useSelector(state => ({
    loading: state.group.loading,
    error: state.group.error,
    group: state.group.group,
    username: getUsername(state)
  }));

  useEffect(() => {
    dispatch(crudThunk.getUserGroup(username));
  }, [dispatch]);

  const renderGroup = () => {
    return group.map(d => {
      return <GroupCard name={d.name} code={d.code} key={d.id} id={d.id} />;
    });
  };

  return (
    <Container>
      <h1>Group List</h1>
      {error && <h1>{error}</h1>}
      {loading && <h1>Loading...</h1>}
      {!loading && <List>{group.length > 0 && renderGroup()}</List>}
    </Container>
  );
};

export default GroupList;
