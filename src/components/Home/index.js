import React, { useState } from "react";
import styled from "styled-components";
import GroupList from "./GroupList";
import ActionButton from "./ActionButton";
import CreateGroupForm from "./CreateGroupForm";
import JoinGroupForm from "./JoinGroupForm";

const Container = styled.div`
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Home = () => {
  const [isFormOpen, setIsFormOpen] = useState(0);

  const openForm = val => {
    setIsFormOpen(val);
  };

  const closeForm = () => {
    setIsFormOpen(0);
  };

  return (
    <Container>
      <GroupList />
      <ActionButton handler={openForm} />
      {isFormOpen === 1 && <CreateGroupForm close={closeForm} />}
      {isFormOpen === 2 && <JoinGroupForm close={closeForm} />}
    </Container>
  );
};

export default Home;
