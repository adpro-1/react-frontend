import React from "react";
import { Container, HalfBox } from "./style";

const ActionButton = props => {
  return (
    <Container>
      <HalfBox onClick={() => props.handler(1)}>
        <h3>Create Group</h3>
      </HalfBox>
      <div className="line"></div>
      <HalfBox onClick={() => props.handler(2)}>
        <h3>Join Group</h3>
      </HalfBox>
    </Container>
  );
};

export default ActionButton;
