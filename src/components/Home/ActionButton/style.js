import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  z-index: 5;
  background-color: #ffbb00;
  box-shadow: 0px 7px 4px rgba(0, 0, 0, 0.15);
  width: 400px;
  height: 70px;
  bottom: 50px;
  border-radius: 20px;
  display: flex;
  align-items: center;
  color: white;

  .line {
    width: 1px;
    background-color: white;
    height: 90%;
  }
`;

export const HalfBox = styled.div`
  width: 50%;
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-grow: 1;
`;
