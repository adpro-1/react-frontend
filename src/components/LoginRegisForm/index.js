import React, { useState } from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

const Container = styled.div`
  padding: 100px;
  display: flex;
  justify-content: center;
`;

const Box = styled.div`
  display: flex;
  flex-direction: column;
`;

const LinkButton = styled.button`
  background: none;
  border: none;
  color: blue;
  cursor: pointer;
`;

const LoginRegisForm = () => {
  const [isLogin, setIsLogin] = useState(true);
  const { loadingLogin, loadingRegister } = useSelector(state => ({
    loadingLogin: state.auth.loadingLogin,
    loadingRegister: state.auth.loadingRegister
  }));

  return (
    <Container>
      {isLogin && (
        <Box>
          <h1>Login</h1>
          {loadingLogin && <span>Authenticating...</span>}
          {!loadingLogin && <LoginForm />}
          <span>
            Don&apos;t have an account?
            <LinkButton onClick={() => setIsLogin(false)}>Register</LinkButton>
          </span>
        </Box>
      )}
      {!isLogin && (
        <Box>
          <h1>Register</h1>
          {loadingRegister && <span>Registering new Account...</span>}
          {!loadingRegister && <RegisterForm />}
          <span>
            Already have an account?
            <LinkButton onClick={() => setIsLogin(true)}>Login</LinkButton>
          </span>
        </Box>
      )}
    </Container>
  );
};

export default LoginRegisForm;
