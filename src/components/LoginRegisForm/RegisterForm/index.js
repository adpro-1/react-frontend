import React from "react";
import { RegisterFormContainer } from "./style";
import Form from "../../Form";
import RegisterButton from "./RegisterButton";

class RegisterForm extends React.Component {
  state = {
    name: "",
    username: "",
    email: "",
    password: "",
    error: ""
  };

  emailHandler = email => {
    this.setState({
      email
    });
  };

  passwordHandler = password => {
    this.setState({
      password
    });
  };

  usernameHandler = username => {
    this.setState({
      username
    });
  };

  nameHandler = name => {
    this.setState({
      name
    });
  };

  errorHandler = error => {
    this.setState({
      error
    });
  };

  render() {
    const { name, username, email, password, error } = this.state;

    return (
      <RegisterFormContainer>
        <section>
          <Form label="Name" type="text" handler={this.nameHandler} />
          <Form label="Username" type="text" handler={this.usernameHandler} />
          <Form label="Email" type="email" handler={this.emailHandler} />
          <Form
            label="Password"
            type="password"
            handler={this.passwordHandler}
          />
          <span>{error}</span>
          <RegisterButton
            name={name}
            username={username}
            email={email}
            password={password}
            errorHandler={this.errorHandler}
          />
        </section>
      </RegisterFormContainer>
    );
  }
}

RegisterForm.propTypes = {};

export default RegisterForm;
