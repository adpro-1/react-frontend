import React from "react";
import { connect } from "react-redux";
import { registerThunk } from "../../../../modules/auth/register/thunk";
import { RegisterButtonContainer } from "./style";
import Button from "../../../Button";

class RegisterButton extends React.Component {
  register = () => {
    const { name, username, email, password, register } = this.props;
    register(name, username, email, password);
  };

  componentDidUpdate = prevProps => {
    const { error, errorHandler } = this.props;

    if (prevProps.error !== error) {
      errorHandler(error);
    }
  };

  render() {
    return (
      <RegisterButtonContainer>
        <Button onClick={this.register} value="Register" />
      </RegisterButtonContainer>
    );
  }
}

RegisterButton.propTypes = {};

const mapStateToProps = state => {
  return {
    error: state.auth.errorRegister
  };
};

const mapDispatchToProps = dispatch => {
  return {
    register: (name, username, email, password) => {
      dispatch(registerThunk.register(name, username, email, password));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterButton);
