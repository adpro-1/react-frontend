import React from "react";
import { connect } from "react-redux";
import { LoginButtonContainer } from "./style";
import { loginThunk } from "../../../../modules/auth/login/thunk";
import Button from "../../../Button";

class LoginButton extends React.Component {
  login = () => {
    const { email, password, login } = this.props;
    login(email, password);
  };

  componentDidUpdate = prevProps => {
    const { error, errorHandler } = this.props;

    if (prevProps.error !== error) {
      errorHandler(error);
    }
  };

  render() {
    return (
      <LoginButtonContainer>
        <Button onClick={this.login} value="Login" />
      </LoginButtonContainer>
    );
  }
}

LoginButton.propTypes = {};

const mapStateToProps = state => {
  return {
    error: state.auth.errorLogin
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: (email, password) => {
      dispatch(loginThunk.login(email, password));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginButton);
