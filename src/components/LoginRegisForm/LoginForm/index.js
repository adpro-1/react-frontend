import React from "react";
import { LoginFormContainer } from "./style";
import Form from "../../Form";
import LoginButton from "./LoginButton";

class LoginForm extends React.Component {
  state = {
    email: "",
    password: "",
    error: ""
  };

  emailHandler = email => {
    this.setState({
      email
    });
  };

  passwordHandler = password => {
    this.setState({
      password
    });
  };

  errorHandler = error => {
    this.setState({
      error
    });
  };

  render() {
    const { email, password, error } = this.state;
    return (
      <LoginFormContainer>
        <form>
          <Form label="Email" type="text" handler={this.emailHandler} />
          <Form
            label="Password"
            type="password"
            handler={this.passwordHandler}
          />
          <span>{error}</span>
          <LoginButton
            email={email}
            password={password}
            errorHandler={this.errorHandler}
          />
        </form>
      </LoginFormContainer>
    );
  }
}

LoginForm.propTypes = {};

export default LoginForm;
