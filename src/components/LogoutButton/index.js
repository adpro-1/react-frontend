import React from "react";
import { connect } from "react-redux";
import { LogoutButtonContainer } from "./style";
import { loginThunk } from "../../modules/auth/login/thunk";
import Button from "../Button";
import logoutLogo from "../../static/log-out.svg";

class LogoutButton extends React.Component {
  logout = () => {
    const { logout, state } = this.props;
    logout(state);
  };

  render() {
    return (
      <LogoutButtonContainer>
        <button className="mobile" type="button" onClick={this.logout}>
          <img alt="logout" src={logoutLogo} />
        </button>
        <Button className="desktop" onClick={this.logout} value="logout" />
      </LogoutButtonContainer>
    );
  }
}

LogoutButton.propTypes = {};

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: state => {
      dispatch(loginThunk.logout(state));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoutButton);
