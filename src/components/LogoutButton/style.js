import styled from "styled-components";

export const LogoutButtonContainer = styled.div`
  display: flex;
  button {
    background: none;
    border: none;
  }

  .mobile {
    display: none;
  }

  @media only screen and (max-width: 768px) {
    button {
      display: none;
    }

    .mobile {
      display: flex;
    }

    .desktop {
      display: none;
    }
  }
`;
