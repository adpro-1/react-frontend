import styled from "styled-components";

export const Container = styled.header`
  padding-left: 100px;
  padding-right: 100px;
  padding-top: 10px;
  padding-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.25);

  a {
    text-decoration: none;
  }
  h1 {
    padding: 0;
    margin: 0;
  }

  .mobile {
    display: none;
  }

  @media only screen and (max-width: 768px) {
    padding: 10px;

    .desktop {
      display: none;
    }

    .mobile {
      display: flex;
    }
  }
`;
