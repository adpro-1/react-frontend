import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Container } from "./style";
import LogoutButton from "../LogoutButton";
import { isLoggedIn, getUsername } from "../../modules/auth/login/selectors";
import bell from "../../static/bell.svg";
import user from "../../static/user.svg";

const Menu = styled.div`
  text-decoration: none;
  display: flex;
  a {
    color: black;
  }
  margin: 5px 50px;
  align-items: center;
  justify-content: space-around;

  @media only screen and (max-width: 768px) {
    margin: 5px 30px;
  }
`;

const Navbar = () => {
  const state = useSelector(prop => prop);

  return (
    <Container>
      <Menu>
        <Link to="/">
          <h1>siBen</h1>
        </Link>
      </Menu>
      {isLoggedIn(state) && (
        <>
          <Menu className="desktop">
            <Link to="/tagihan">Tagihan</Link>
            <Menu>{getUsername(state)}</Menu>
            <LogoutButton value="button" />
          </Menu>
          <Menu className="mobile">
            <Link to="/tagihan">
              <img alt="bell" src={bell} />
            </Link>
            <Menu>
              <img alt="user" src={user} />
            </Menu>
            <LogoutButton />
          </Menu>
        </>
      )}
    </Container>
  );
};

export default Navbar;
