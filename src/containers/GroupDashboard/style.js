import styled from "styled-components";

export const GroupDashboardContainer = styled.div`
  padding-top: 40px;
  padding-left: 100px;
  padding-right: 100px;
  padding-bottom: 40px;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media only screen and (max-width: 900px) {
    padding: 30px;
  }
`;
