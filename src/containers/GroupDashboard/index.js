import React from "react";
import { connect } from "react-redux";
import { isLoggedIn, getUsername } from "../../modules/auth/login/selectors";
import { GroupDashboardContainer } from "./style";
import Group from "../../components/Group";
import { crudThunk } from "../../modules/group/crud/thunk";

class GroupDashboard extends React.Component {
  componentWillMount = () => {
    const { match, username, getGroupDetail } = this.props;
    const { id } = match.params;
    getGroupDetail(username, id);
  };

  render() {
    const { match, username } = this.props;
    const { id } = match.params;

    return (
      <GroupDashboardContainer>
        <Group
          data={{
            username,
            id
          }}
        />
      </GroupDashboardContainer>
    );
  }
}

GroupDashboard.propTypes = {};

function mapStateToProps(state) {
  return {
    username: getUsername(state),
    isLoggedIn: isLoggedIn(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getGroupDetail: (username, groupId) =>
      dispatch(crudThunk.getDetailGroup(username, groupId))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupDashboard);
