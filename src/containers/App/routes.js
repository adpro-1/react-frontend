import LandingPage from "../LandingPage";
import GroupDashboard from "../GroupDashboard";
import TuitionDashboard from "../TuitionDashboard";

export const routes = [
  {
    component: LandingPage,
    exact: true,
    path: "/"
  },
  {
    component: GroupDashboard,
    exact: true,
    path: "/:id/:groupName"
  },
  {
    component: TuitionDashboard,
    exact: true,
    path: "/tagihan"
  }
];
