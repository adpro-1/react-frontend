import React from "react";
import { connect } from "react-redux";
import { TuitionDashboardContainer } from "./style";
import { isLoggedIn, getUsername } from "../../modules/auth/login/selectors";
import { crudThunk } from "../../modules/group/crud/thunk";
import Tuition from "../../components/Tuition";

class TuitionDashboard extends React.Component {
  componentWillMount = () => {
    const { username, getUserGroup } = this.props;
    getUserGroup(username);
  };

  render() {
    const { username } = this.props;
    return (
      <TuitionDashboardContainer>
        <h1>List Tagihan</h1>
        <Tuition username={username} />
      </TuitionDashboardContainer>
    );
  }
}

TuitionDashboard.propTypes = {};

function mapStateToProps(state) {
  return {
    username: getUsername(state),
    isLoggedIn: isLoggedIn(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getUserGroup: username => dispatch(crudThunk.getUserGroup(username))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TuitionDashboard);
