import React from "react";
import { connect } from "react-redux";
import { LandingPageContainer } from "./style";
import { isLoggedIn } from "../../modules/auth/login/selectors";
import LoginRegisForm from "../../components/LoginRegisForm";
import Home from "../../components/Home";

class LandingPage extends React.Component {
  render() {
    // eslint-disable-next-line no-shadow
    const { isLoggedIn } = this.props;

    return (
      <LandingPageContainer>
        <section>{isLoggedIn ? <Home /> : <LoginRegisForm />}</section>
      </LandingPageContainer>
    );
  }
}

LandingPage.propTypes = {};

function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
