import { loginAction } from "./constant";
import sibenApi from "../../../api/siben";
import { isLoggedIn } from "./selectors";
import { setAuthToken } from "../../../api/http";

export const loginThunk = {
  login: (email, password) => {
    return dispatch => {
      dispatch(loginAction.startLogin());
      const login = sibenApi.auth.login(email, password);
      login
        .then(res => {
          setAuthToken(res.data.accessToken);
          dispatch(loginAction.successLogin(res.data));
        })
        .catch(err => {
          dispatch(loginAction.failLogin(err.response.data.message));
        });
      setTimeout(() => {
        dispatch(loginAction.finishLogin());
      }, 2000);
    };
  },
  logout: state => {
    return dispatch => {
      dispatch(loginAction.startLogin());
      if (isLoggedIn(state)) {
        dispatch(loginAction.successLogout());
      } else {
        dispatch(loginAction.failLogin("unauthorized request"));
      }
      dispatch(loginAction.finishLogin());
      localStorage.clear();
      window.location.href = "/";
    };
  }
};
