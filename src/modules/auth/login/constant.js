const BASE = "src/login/";

export const START_LOGIN = `${BASE}start`;
export const FAIL_LOGIN = `${BASE}fail`;
export const SUCCESS_LOGIN = `${BASE}success`;
export const FINISH_LOGIN = `${BASE}finish`;
export const SUCCESS_LOGOUT = `${BASE}success/logout`;

export const loginAction = {
  startLogin: () => {
    return {
      type: START_LOGIN
    };
  },
  failLogin: error => {
    return {
      type: FAIL_LOGIN,
      error
    };
  },
  successLogin: payload => {
    return {
      type: SUCCESS_LOGIN,
      payload
    };
  },
  finishLogin: () => {
    return {
      type: FINISH_LOGIN
    };
  },
  successLogout: () => {
    return {
      type: SUCCESS_LOGOUT
    };
  }
};

export default loginAction;
