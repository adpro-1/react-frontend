import { setAuthToken } from "../../../api/http";

export function getUserToken(state) {
  return state.auth.user.token;
}

export function getUsername(state) {
  return state.auth.user.username;
}

export function isLoggedIn(state) {
  const token = getUserToken(state);
  setAuthToken(token);
  return !!token;
}
