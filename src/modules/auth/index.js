import {
  START_LOGIN,
  FAIL_LOGIN,
  SUCCESS_LOGIN,
  FINISH_LOGIN,
  SUCCESS_LOGOUT
} from "./login/constant";
import {
  START_REGISTER,
  FAIL_REGISTER,
  SUCCESS_REGISTER,
  FINISH_REGISTER
} from "./register/constant";

const initialState = {
  user: {
    token: undefined,
    username: undefined
  },
  loadingLogin: false,
  errorLogin: undefined,
  loadingRegister: false,
  errorRegister: undefined
};

const progresLoginState = {
  startLogin: state => {
    return {
      ...state,
      loadingLogin: true
    };
  },
  failLogin: (state, action) => {
    return {
      ...state,
      errorLogin: action.error
    };
  },
  successLogin: (state, action) => {
    return {
      ...state,
      user: {
        token: action.payload.accessToken,
        username: action.payload.username
      }
    };
  },
  finishLogin: state => {
    return {
      ...state,
      loadingLogin: false,
      errorLogin: undefined
    };
  },
  successLogout: state => {
    return {
      ...state,
      user: {
        token: undefined,
        username: undefined
      }
    };
  }
};

const progresRegisterState = {
  startRegister: state => {
    return {
      ...state,
      loadingRegister: true
    };
  },
  failRegister: (state, action) => {
    return {
      ...state,
      errorRegister: action.error
    };
  },
  successRegister: state => {
    return {
      ...state
    };
  },
  finishRegister: state => {
    return {
      ...state,
      loadingRegister: false,
      errorRegister: undefined
    };
  }
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_LOGIN:
      return progresLoginState.startLogin(state);
    case SUCCESS_LOGIN:
      return progresLoginState.successLogin(state, action);
    case FAIL_LOGIN:
      return progresLoginState.failLogin(state, action);
    case FINISH_LOGIN:
      return progresLoginState.finishLogin(state);
    case SUCCESS_LOGOUT:
      return progresLoginState.successLogout(state);
    case START_REGISTER:
      return progresRegisterState.startRegister(state);
    case SUCCESS_REGISTER:
      return progresRegisterState.successRegister(state);
    case FAIL_REGISTER:
      return progresRegisterState.failRegister(state, action);
    case FINISH_REGISTER:
      return progresRegisterState.finishRegister(state);
    default:
      return state;
  }
};

export default authReducer;
