import { registerAction } from "./constant";
import sibenApi from "../../../api/siben";
import { loginThunk } from "../login/thunk";

export const registerThunk = {
  register: (name, username, email, password) => {
    return dispatch => {
      dispatch(registerAction.startRegister());
      const register = sibenApi.auth.signup(name, username, email, password);
      register
        .then(res => {
          dispatch(registerAction.successRegister(res.data));
          // login the new user
          dispatch(loginThunk.login(email, password));
        })
        .catch(err => {
          dispatch(registerAction.failRegister(err.response.data.message));
        });
      setTimeout(() => {
        dispatch(registerAction.finishRegister());
      }, 1000);
    };
  }
};
