const BASE = "src/Register/";

export const START_REGISTER = `${BASE}start`;
export const FAIL_REGISTER = `${BASE}fail`;
export const SUCCESS_REGISTER = `${BASE}success`;
export const FINISH_REGISTER = `${BASE}finish`;

export const registerAction = {
  startRegister: () => {
    return {
      type: START_REGISTER
    };
  },
  failRegister: error => {
    return {
      type: FAIL_REGISTER,
      error
    };
  },
  successRegister: payload => {
    return {
      type: SUCCESS_REGISTER,
      payload
    };
  },
  finishRegister: () => {
    return {
      type: FINISH_REGISTER
    };
  }
};

export default registerAction;
