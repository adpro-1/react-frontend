import {
  START_CRUD,
  FAIL_CRUD,
  SUCCESS_CRUD,
  FINISH_CRUD
} from "./crud/constant";

import {
  START_REQUEST,
  FAIL_REQUEST,
  SUCCESS_REQUEST,
  FINISH_REQUEST
} from "./request/constant";

const initialState = {
  group: [],
  groupDetail: {},
  tuition: [],
  loading: false,
  success: undefined,
  error: undefined,
  request: {
    membershipRequest: [],
    budgetRequest: []
  },
  loadingRequest: false
};

const progressState = {
  start: state => {
    return {
      ...state,
      loading: true
    };
  },
  fail: (state, action) => {
    return {
      ...state,
      loading: false,
      error: action.error
    };
  },
  success: (state, action) => {
    switch (action.payload.type) {
      case "ALL":
        return {
          ...state,
          group: action.payload.data.groups,
          tuition: action.payload.data.tuition,
          success: action.payload.msg
        };
      case "DETAIL":
        return {
          ...state,
          groupDetail: action.payload.data,
          success: action.payload.msg
        };
      case "DELETE":
        return {
          ...state,
          success: action.payload.msg
        };
      default:
        return { ...state };
    }
  },
  finish: state => {
    return {
      ...state,
      success: undefined,
      error: undefined,
      loading: false
    };
  }
};

const requestProgresState = {
  start: state => {
    return {
      ...state,
      loadingRequest: true
    };
  },
  success: (state, action) => {
    if (action.payload.membershipRequest) {
      return {
        ...state,
        request: action.payload
      };
    }
    return {
      ...state,
      request: {
        membershipRequest: [],
        requestBudget: action.payload
      }
    };
  },
  fail: (state, action) => {
    return {
      ...state,
      error: action.error
    };
  },
  finish: state => {
    return {
      ...state,
      loadingRequest: false
    };
  }
};

const groupReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_CRUD:
      return progressState.start(state);
    case FAIL_CRUD:
      return progressState.fail(state, action);
    case SUCCESS_CRUD:
      return progressState.success(state, action);
    case FINISH_CRUD:
      return progressState.finish(state);
    case START_REQUEST:
      return requestProgresState.start(state);
    case FAIL_REQUEST:
      return requestProgresState.fail(state, action);
    case SUCCESS_REQUEST:
      return requestProgresState.success(state, action);
    case FINISH_REQUEST:
      return requestProgresState.finish(state);
    default:
      return state;
  }
};

export default groupReducer;
