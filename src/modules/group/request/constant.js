const BASE = "src/group/request/";

export const START_REQUEST = `${BASE}start`;
export const FAIL_REQUEST = `${BASE}fail`;
export const SUCCESS_REQUEST = `${BASE}success`;
export const FINISH_REQUEST = `${BASE}finish`;

export const RequestAction = {
  startRequest: () => {
    return {
      type: START_REQUEST
    };
  },
  failRequest: error => {
    return {
      type: FAIL_REQUEST,
      error
    };
  },
  successRequest: payload => {
    return {
      type: SUCCESS_REQUEST,
      payload
    };
  },
  finishRequest: () => {
    return {
      type: FINISH_REQUEST
    };
  }
};

export default RequestAction;
