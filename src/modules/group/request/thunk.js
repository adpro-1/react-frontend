import { RequestAction } from "./constant";
import sibenApi from "../../../api/siben";
import { crudThunk } from "../crud/thunk";

export const requestThunk = {
  createMembershipRequest: (username, code) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.createMembershipRequest(username, code);
      body
        .then(res => {
          console.log(res.data);
        })
        .catch(err => {
          console.log(err.response);
        });
    };
  },
  approveMembershipRequest: (username, id, status, groupId) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.approveMembershipRequest(
        username,
        id,
        status
      );
      body
        .then(res => {
          console.log(res.data);
          dispatch(requestThunk.getGroupRequest(username, groupId));
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          console.log(err.response);
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  inviteMember: (username, groupId, usernameToInvite) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.inviteMember(
        username,
        groupId,
        usernameToInvite
      );
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  inviteAdmin: (username, groupId, usernameToInvite) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.inviteAdmin(
        username,
        groupId,
        usernameToInvite
      );
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  requestBudget: (username, code, amount, groupId) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.requestBudget(username, code, amount);
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
          dispatch(requestThunk.getGroupRequest(username, groupId));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  approveRequestBudget: (username, id, status, groupId) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.approveRequestBudget(username, id, status);
      body
        .then(res => {
          console.log(res.data);
          dispatch(requestThunk.getGroupRequest(username, groupId));
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          console.log(err.response);
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  getGroupRequest: (username, groupId) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.getGroupRequest(username, groupId);
      body
        .then(res => {
          console.log(res);
          dispatch(RequestAction.successRequest(res.data));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  payTuition: (username, idTuition) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.payTuition(username, idTuition);
      body
        .then(() => {
          dispatch(crudThunk.getUserGroup(username));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  verifyPayTuition: (username, idTuition, groupId) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.verifyPaidTuition(username, idTuition);
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  },
  notifyTuition: (username, idTuition, groupId) => {
    return dispatch => {
      dispatch(RequestAction.startRequest());
      const body = sibenApi.group.notifyTuition(username, idTuition);
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(RequestAction.failRequest(err.response));
        });
      dispatch(RequestAction.finishRequest());
    };
  }
};
