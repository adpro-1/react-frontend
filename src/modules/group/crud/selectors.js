export const getGroupByName = (name, state) => {
  return state.group.group.filter(group => {
    return group.name === name;
  });
};

export const cekIsAdmin = (group, username) => {
  const admin = group.admins.filter(user => {
    return user.username === username;
  });

  return admin.length > 0;
};
