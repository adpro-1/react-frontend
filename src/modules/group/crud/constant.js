const BASE = "src/group/crud/";

export const START_CRUD = `${BASE}start`;
export const FAIL_CRUD = `${BASE}fail`;
export const SUCCESS_CRUD = `${BASE}success`;
export const FINISH_CRUD = `${BASE}finish`;

export const CRUDAction = {
  startCRUD: () => {
    return {
      type: START_CRUD
    };
  },
  failCRUD: error => {
    return {
      type: FAIL_CRUD,
      error
    };
  },
  successCRUD: payload => {
    return {
      type: SUCCESS_CRUD,
      payload
    };
  },
  finishCRUD: () => {
    return {
      type: FINISH_CRUD
    };
  }
};

export default CRUDAction;
