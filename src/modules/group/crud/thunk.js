import { CRUDAction } from "./constant";
import sibenApi from "../../../api/siben";

export const crudThunk = {
  createGroup: (username, name, budget) => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.createGroup(username, name, budget);
      body
        .then(() => {
          dispatch(crudThunk.getUserGroup(username));
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  },
  deleteGroup: (username, groupId) => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.deleteGroup(username, groupId);
      body
        .then(() => {
          dispatch(
            CRUDAction.successCRUD({
              type: "DELETE"
            })
          );
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  },
  getUserGroup: username => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.getUserGroup(username);
      body
        .then(res => {
          dispatch(
            CRUDAction.successCRUD({
              data: res.data,
              type: "ALL"
            })
          );
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response.data.error));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  },
  getDetailGroup: (username, groupId) => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.getGroupDetail(username, groupId);
      body
        .then(res => {
          dispatch(
            CRUDAction.successCRUD({
              data: res.data,
              type: "DETAIL"
            })
          );
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  },
  removeMember: (username, groupId, usernameToRemove) => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.removeMember(
        username,
        groupId,
        usernameToRemove
      );
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  },
  editBudget: (username, groupId, budget) => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.editBudget(username, groupId, budget);
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  },
  createTuitiom: (username, groupId, title, nominal) => {
    return dispatch => {
      dispatch(CRUDAction.startCRUD());
      const body = sibenApi.group.createTuition(
        username,
        groupId,
        title,
        nominal
      );
      body
        .then(() => {
          dispatch(crudThunk.getDetailGroup(username, groupId));
        })
        .catch(err => {
          dispatch(CRUDAction.failCRUD(err.response));
        });
      dispatch(CRUDAction.finishCRUD());
    };
  }
};
