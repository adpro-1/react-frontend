/* eslint-disable */
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import globalReducer from "./../globalReducer";
import authReducer from "./auth";
import groupReducer from "./group";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

export default history =>
  combineReducers({
    router: connectRouter(history),
    global: globalReducer,
    auth: persistReducer(
      { key: "auth", storage, whitelist: ["user"] },
      authReducer
    ),
    group: persistReducer(
      { key: "group", storage, whitelist: ["group", "groupDetail", "tuition"] },
      groupReducer
    )
  });
