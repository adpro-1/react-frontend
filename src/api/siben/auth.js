import axios from "axios";
import constant from "../constants";

export default {
  // create session
  login: (email, password) =>
    axios.post(`${constant.API_BASE_URL}auth/signin/`, {
      email,
      password
    }),
  // register
  signup: (name, username, email, password) =>
    axios.post(`${constant.API_BASE_URL}auth/register/`, {
      name,
      username,
      email,
      password
    })
};
