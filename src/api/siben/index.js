import auth from "./auth";
import group from "./group";

const sibenApi = {
  auth,
  group
};

export default sibenApi;
