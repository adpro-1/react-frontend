import axios from "axios";
import constant from "../constants";

export default {
  createGroup: (username, name, budget) =>
    axios.post(`${constant.API_BASE_URL}group`, {
      username,
      name,
      budget
    }),
  deleteGroup: (username, groupId) =>
    axios.delete(`${constant.API_BASE_URL}group/delete/${groupId}/${username}`),
  getGroupDetail: (username, groupId) =>
    axios.get(`${constant.API_BASE_URL}group/detail/${groupId}/${username}`),
  getUserGroup: username =>
    axios.get(`${constant.API_BASE_URL}group/${username}`),
  createMembershipRequest: (username, code) =>
    axios.post(`${constant.API_BASE_URL}group/request-membership`, {
      username,
      code
    }),
  approveMembershipRequest: (username, idRequest, status) =>
    axios.post(`${constant.API_BASE_URL}group/approve-membership`, {
      username,
      status,
      id: idRequest
    }),
  getGroupRequest: (username, groupId) =>
    axios.get(
      `${constant.API_BASE_URL}group/detail/request/${groupId}/${username}`
    ),
  removeMember: (username, groupId, usernameToRemove) =>
    axios.delete(
      `${constant.API_BASE_URL}group/detail/remove-member/${username}/${groupId}/${usernameToRemove}`
    ),
  inviteMember: (username, groupId, usernameToInvite) =>
    axios.post(
      `${constant.API_BASE_URL}group/detail/invite-member/${username}/${groupId}/${usernameToInvite}`
    ),
  inviteAdmin: (username, groupId, usernameToInvite) =>
    axios.post(
      `${constant.API_BASE_URL}group/detail/invite-admin/${username}/${groupId}/${usernameToInvite}`
    ),
  requestBudget: (username, code, amount) =>
    axios.post(`${constant.API_BASE_URL}group/request-budget`, {
      username,
      code,
      amount
    }),
  approveRequestBudget: (username, idRequest, status) =>
    axios.post(`${constant.API_BASE_URL}group/approve-request-budget`, {
      username,
      status,
      id: idRequest
    }),
  editBudget: (username, groupId, budget) =>
    axios.patch(`${constant.API_BASE_URL}group/detail/edit-budget/${groupId}`, {
      username,
      budget
    }),
  createTuition: (username, groupId, title, nominal) =>
    axios.post(`${constant.API_BASE_URL}group/detail/create-tuition`, {
      username,
      groupId,
      nominal,
      title
    }),
  payTuition: (username, idTuition) =>
    axios.patch(`${constant.API_BASE_URL}group/detail/paid-tuition`, {
      username,
      id: idTuition
    }),
  verifyPaidTuition: (username, idTuition) =>
    axios.patch(`${constant.API_BASE_URL}group/detail/verify-paid-tuition`, {
      username,
      id: idTuition
    }),
  notifyTuition: (username, idTuition) =>
    axios.post(`${constant.API_BASE_URL}group/detail/notify-tuition`, {
      username,
      id: idTuition
    })
  // lanjutt
};
