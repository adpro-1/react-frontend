export const isDevelopment =
  process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test";

const developmentConstants = {
  API_BASE_URL: "http://localhost:8080/api/"
};

const productionConstants = {
  API_BASE_URL: "https://siben-api.herokuapp.com/api/"
};

export default isDevelopment ? developmentConstants : productionConstants;
